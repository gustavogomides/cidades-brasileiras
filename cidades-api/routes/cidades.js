var express = require('express');
var router = express.Router();

var Cidades = require('../models/Cidades.js');

/* GET /cidades */
router.get('/', function(req, res, next) {
  Cidades.find(function (err, cidades) {
    if (err) 
      return next(err);
    res.json(cidades);
  });
});

/* GET /cidades/:id */
router.get('/:id', function(req, res, next) {
  Cidades.findById(req.params.id, function (err, post) {
    if (err)
      return next(err);
    res.json(post);
  });
});

/* POST /cidades */
router.post('/', function(req, res, next) {
  Cidades.create(req.body, function (err, post) {
    if (err) 
      return next(err);
    res.json(post);
  });
});

/* PUT /cidades/:id */
router.put('/:id', function(req, res, next) {
  Cidades.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) 
      return next(err);
    res.json(post);
  });
});

/* DELETE /cidades/:id */
router.delete('/:id', function(req, res, next) {
  Cidades.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) 
      return next(err);
    res.json(post);
  });
});

module.exports = router;