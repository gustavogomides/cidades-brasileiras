console.log('Iniciando node...');

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

// CARREGANDO O MÓDULO mongoose
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;

// CONEXÃO COM O BD
mongoose.connect('mongodb://localhost/cidades-api')
  .then(() =>  console.log('MongoDB - Conexão realizada com sucesso!'))
  .catch((err) => console.error(err));
  
var app = express();

// ESCUTANDO DA PORTA 4000
app.listen(4000);
console.log('Porta 4000 setada!')

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// ICON
app.use('/public/images/favicon.ico', function(req, res){
  res.sendFile(__dirname + '/public/images/favicon.ico');
});

// LIBS
app.use('/libs/angular.min.js', function(req, res){
  res.sendFile(__dirname + '/libs/angular.min.js');
}); 

app.use('/libs/angular-route.min.js', function(req, res){
  res.sendFile(__dirname + '/libs/angular-route.min.js');
});

app.use('/libs/angular-resource.min.js', function(req, res){
  res.sendFile(__dirname + '/libs/angular-resource.min.js');
});

// HOME
app.use('/', require('./routes/index'));

// MODEL
app.use('/cidades', require('./routes/cidades'));

// VIEWS
app.use('/cadastrar', function(req, res){
  res.sendFile(__dirname + '/views/cadastrar.html');
});

app.use('/listar', function(req, res){
  res.sendFile(__dirname + '/views/listar.html');
});

// CONTROLLERS
app.use('/controllers/MainController.js', function(req, res){
  res.sendFile(__dirname + '/controllers/MainController.js');
});

app.use('/controllers/ListarController.js', function(req, res){
  res.sendFile(__dirname + '/controllers/ListarController.js');
});

app.use('/controllers/CadastrarController.js', function(req, res){
  res.sendFile(__dirname + '/controllers/CadastrarController.js');
});

app.use('/controllers/AtualizarController.js', function(req, res){
  res.sendFile(__dirname + '/controllers/AtualizarController.js');
});

app.use('/atualizar', function(req, res){
  res.sendFile(__dirname + '/views/atualizar.html');
});

// PÁGINA NÃO ENCONTRADA
app.use(function(req, res, next) {
  var err = new Error('Página não encontrada!');
  err.status = 404;
  next(err);
});

// ERRO
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;