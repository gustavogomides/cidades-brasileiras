angular.module('app').controller('ListarController', ['$scope', 'Cidades', '$location', function ($scope, Cidades, $location) {
	$scope.cidades = Cidades.query();

	$scope.voltar = function() {
		$location.url('/');
	};

	$scope.remover = function(nome, uf){
		var cidade;

		// recuperando a cidade cadastrada de acordo com o nome e a UF
		for(var i = 0; i < $scope.cidades.length; i++){
			if((new String($scope.cidades[i].nome).valueOf() == new String(nome).valueOf()) && 
				(new String($scope.cidades[i].unidadefederativa).valueOf() == new String(uf).valueOf())) {
				cidade = $scope.cidades[i];
				break;
			}
		}

		if(confirm("A cidade '" + cidade.nome +"' na Unidade Federativa '"+ cidade.unidadefederativa 
			+ "' será removida. Você tem certeza?")) {
			Cidades.remove({id: cidade._id}, function(){
				window.location.reload();
			});
		}
	}
}])

