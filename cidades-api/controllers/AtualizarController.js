angular.module('app').controller('AtualizarController', ['$scope', '$routeParams', 'Cidades', '$location', function ($scope, $routeParams, Cidades, $location) {
	
	// obtendo as informações da cidade escolhida
	$scope.cidade = Cidades.get({id: $routeParams.id });

	$scope.update = function(){
		Cidades.update({id: $scope.cidade._id}, $scope.cidade, function(){
			window.alert("Cidade '" + $scope.cidade.nome + "' atualizada com sucesso!");
			$location.url('/listar');
		});
	}

	$scope.voltar = function() {
		$location.url('/listar');
	};
}]);
