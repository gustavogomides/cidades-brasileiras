angular.module('app').controller('CadastrarController', ['$scope', 'Cidades', '$location', function ($scope, Cidades, $location) {      
	
	// obtendo todas as cidades cadastradas para a posterior verificação de existência
	$scope.cidades = Cidades.query();

	$scope.salvar = function(){
		var jaExiste = false;
		
		// verificando se a cidade na determinada UF já foi cadastrada anteriormente
		for(var i = 0; i < $scope.cidades.length; i++){
			if((new String($scope.cidades[i].nome).valueOf() == new String($scope.modelNome).valueOf()) && 
			(new String($scope.cidades[i].unidadefederativa).valueOf() == new String($scope.modelUF).valueOf())){
				jaExiste = true;
				break;
			}
		}
		if(!jaExiste) { // cidade não foi cadastrada anteriormente
			var cidade = new Cidades({ nome: $scope.modelNome, gentilico: $scope.modelGentilico, 
				lema: $scope.modelLema, prefeitoatual: $scope.modelPrefeito, unidadefederativa: $scope.modelUF, 
				distanciacapital: $scope.modelDistancia, area: $scope.modelArea, altitude: $scope.modelAltitude });
			cidade.$save(function(){
				$scope.cidades.push(cidade);
			});
			window.alert("A cidade '"+ $scope.modelNome + "' foi cadastrada com sucesso!");
			$location.url('/');
		} else{ // cidade já cadastrada anteriormente
			window.alert("A cidade '"+ $scope.modelNome +"' localizada na Unidade Federativa '"+ $scope.modelUF +"' já foi cadastrada anteriormente!");
		}
		
	}
	$scope.voltar = function() {
		$location.url('/');
	};
}]);