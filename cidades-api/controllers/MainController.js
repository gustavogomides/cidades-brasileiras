var app = angular.module('app', ['ngRoute', 'ngResource']);

//    SERVICES  
app.factory('Cidades', ['$resource', function($resource){
	return $resource('/cidades/:id', null, {
		'update': { method:'PUT' }
	});
}])

//    ROTAS
app.config(['$routeProvider', function ($routeProvider) {
	$routeProvider
		.when('/', {
			templateUrl: '/cidades.html'
		})

		.when('/cadastrar', {
			templateUrl: 'cadastrar',
			controller: 'CadastrarController'
		})

		.when('/listar', {
			templateUrl: 'listar',
			controller: 'ListarController'
		})

		.when('/atualizar/:id', {
			templateUrl: 'atualizar',
			controller: 'AtualizarController'
		});
}]);
