var mongoose = require('mongoose');

var SchemaCidade = new mongoose.Schema({
	nome: String, 
	gentilico: String, 
	lema: String, 
	prefeitoatual: String, 
	unidadefederativa: String, 
	distanciacapital: Number, 
	area: Number, 
	altitude: Number,
});

module.exports = mongoose.model('Cidades', SchemaCidade);

